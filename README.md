
To run the application, do the following:

1. Run npm install so that node_modules is created
2. Run npm start

To change the EC2 instance where auth server is pointing, go to the following file.
src/constants/index.js

```
export const API_BASE_URL = 'http://ec2-3-17-29-137.us-east-2.compute.amazonaws.com:8090';
export const ACCESS_TOKEN = 'accessToken';

export const OAUTH2_REDIRECT_URI = 'http://localhost:3000/oauth2/redirect'

export const GOOGLE_AUTH_URL = API_BASE_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const FACEBOOK_AUTH_URL = API_BASE_URL + '/oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const GITHUB_AUTH_URL = API_BASE_URL + '/oauth2/authorize/github?redirect_uri=' + OAUTH2_REDIRECT_URI;

```

Change the base location for API_BASE_URL.


